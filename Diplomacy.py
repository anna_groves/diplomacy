
# armyDict stores each army as a key and its city as a value.
armyDict = {}

# Cities stores each city as a key as a key and all armies present there as a value.
cities = {}

# Supports stores information about which armies are supporting other armies.
supports = {}

# Solve diplomacy
def diplomacy_solve(r, w):
    armies = []
    for i in r:
        assert 3 <= len(i.split()) <= 4
        army = Army(i.split())
        armies.append(army)

    for army in armies:
        army.army_move()

    for city, army in cities.items():
        if len(army) > 1:
           get_battle_outcome(city, army)

    for army in armies:
        output = army.output_print()
        assert len(output.split()) == 2
        w.write(output)

    armyDict.clear()
    cities.clear()
    supports.clear()


# get_battle_outcome determines the result of armies clashing.
def get_battle_outcome(city, armies):
    support = [len(supports.get(i)) for i in armies]

    while len(armies) > 1:
        if min(support) == max(support):
            for i in armies:
                armyDict[i] = "[dead]"

            break

        else:
            idx = support.index(min(support))
            armyDict.update({armies[idx]: "[dead]"})
            armies.remove(armies[idx])
            support.pop(idx)

# A class to hold the name, city, and move of each army
class Army:
    def __init__(self, inputs):
        self.name = name = inputs[0]
        self.city = city = inputs[1]
        self.move = move = inputs[2]

        # Sets the target city/support army if relevant,
        # otherwise, make the current city the target city.
        if len(inputs) == 4:
            self.target = inputs[3]

        else:
            self.target = inputs[1]

        # Input values in dictionaries.
        armyDict[name] = city
        supports[inputs[0]] = []
        cities[city] = [name]

    # army_move updates the dictionaries if an army makes a move requiring it
    # (So anything other than "hold")
    def army_move(self):
        if self.move == "Support":
            supports.get(self.target).append(self.name)

        elif self.move == "Move":

            # If an army moves to a new, unoccupied city, add it to the dictionary.
            if self.target not in cities:
                cities[self.target] = []

            # Set army location to target city
            armyDict[self.name] = self.target

            # Which army holds the target city, if any?
            occupant = cities.get(self.target)
            if occupant:
                occupant = occupant[0]

            # Move army to the appropriate city
            cities.get(self.target).append(self.name)
            cities.get(self.city).remove(self.name)

            # Invalidate support from armies under attack
            for army in supports:
                if occupant in supports.get(army):
                    supports.get(army).remove(occupant)

    # output_print returns correctly formatted output
    def output_print(self):
        return self.name + " " + armyDict.get(self.name)+"\n"
