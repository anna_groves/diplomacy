import sys

from Diplomacy import diplomacy_solve

# ----
# main
# ----

if __name__ == "__main__":
    diplomacy_solve(sys.stdin, sys.stdout)

""" #pragma: no cover
$ cat RunDiplomacy1.in
A Madrid Hold
B Barcelona Move Madrid
C London Support B




$ python RunDiplomacy.py < RunDiplomacy1.in > RunDiplomacy1.out

$ cat RunDiplomacy1.out
A [dead]
B Madrid
C London



$ python -m pydoc -w Diplomacy"
# That creates the file Diplomacy.html
"""
